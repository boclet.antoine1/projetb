//@prepros-prepend ../../node_modules/jquery/dist/jquery.js;
//@prepros-prepend ../../node_modules/lightgallery/dist/js/lightgallery-all.js;
//@prepros-prepend ../../node_modules/masonry-layout/dist/masonry.pkgd.min.js;
//@prepros-prepend ../../node_modules/modal-video/js/jquery-modal-video.js;
//@prepros-prepend ../../node_modules/tingle.js/dist/tingle.min.js;

$(document).ready(function() {
	 $(".js-modal-btn").modalVideo();
     $("#lightgallery").lightGallery({
        selector: '.img-item' ,
        mode: 'lg-zoom-in',
        speed : 750
    });
     $('.grid').masonry({
        columnWitdh: 200,
        itemSelector : '.grid-item',
        isFitWidth: true,
        gutter: 5
    });
 });


// instanciate new modal
var modalTingle = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['button'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
        console.log('modal open');
    },
    onClose: function() {
        console.log('modal closed');
    },
    beforeClose: function() {
        // here's goes some logic
        // e.g. save content before closing the modal
        return true; // close the modal
        return false; // nothing happens
    }
});

var modalTingleEnd = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
        console.log('modal open');
    },
    onClose: function() {
        console.log('modal closed');
    },
    beforeClose: function() {
        // here's goes some logic
        // e.g. save content before closing the modal
        return true; // close the modal
        return false; // nothing happens
    }
});

var error = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
        console.log('modal open');
    },
    onClose: function() {
        console.log('modal closed');
    },
    beforeClose: function() {
        // here's goes some logic
        // e.g. save content before closing the modal
        return true; // close the modal
        return false; // nothing happens
    }
});



$( "#confirm" ).click(function() {
  var nomForm = document.getElementById("nom").value;
  var courrielForm = document.getElementById("courriel").value;

    if (nomForm !== ""){
        if (courrielForm !== ""){
            modalTingle.setContent('<h1>Confirmez vous ces informations ?</h1><br/><ul><li>Nom : '+nomForm+'</li><li>Email : '+courrielForm+'</li></ul>');
            modalTingle.open();
        }
        else{
        error.open();
        }
        
    }
    else{
        error.open();
    }

  
});

error.setContent('<h1>Erreur !</h1></br><h2>Veuillez entrer vos informations.</h2>');
modalTingleEnd.setContent('<h1>Bienvenue !</h1></br><h2>Vous êtes désormais inscrit à la newsletter.</h2>');



// set content

// add a button
modalTingle.addFooterBtn('Modifier', 'tingle-btn tingle-btn--danger', function() {
    // here goes some logic
    modalTingle.close();
});
modalTingle.addFooterBtn('Envoyer', 'tingle-btn tingle-btn--pull-right tingle-btn--primary', function() {
    // here goes some logic
    modalTingle.close();
    modalTingleEnd.open();
});


// open modal
// modalTingle.open();

