<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	
    <title>Bansky</title>
  </head>
  <body>
    <button class="btn-red js-modal-btn" data-video-id="kcRuHVbmcF0">Ouvrir</button>
    <div class="grid">
    		<div id="lightgallery">
	    		<?php for($i=1;$i<=25;$i++): ?> 
                    <div class="grid-item">
		    			<a class='img-item' href="img/<?php echo $i?>.jpg">
				    		<img src="img/<?php echo $i?>.jpg" alt="">
		    			</a>
	    			
	    			</div>
		    	<?php endfor; ?>
    		</div>
        </div>
    


<!-- footer -->
    <footer>
        <form>
            <h1>Inscription Newsletter</h1>
            <div id="centreToiStp">
                
                    
                    <p>
                        <label for="nom">Nom</label> :
                        <input type="text" name="nom" id="nom" required placeholder="Søren Pichot">
                        <span id="aidenom"></span>
                    </p>
                    
                    <p>
                        <label for="courriel">Email</label> :
                        <input type="email" name="courriel" id="courriel" required placeholder="sorenpichot@domaine.fr">
                        <span id="aideCourriel"></span>
                    </p>
                    <p>
                        <input type="checkbox" name="confirmation" id="confirmation">
                        <label for="confirmation">M'envoyer un courriel de confirmation</label>
                    </p>
              
                
                    
                    
                    
                    <button type="button" class="btn btn-info" id="confirm">Validez</button>
                    
                    <button type="reset" class="btn btn-danger">Annuler</button>
                
            </div>
        </form>

    </footer>  

    <script src="js/app.js"></script>
    <script src="../node_modules/tingle.js/dist/tingle.js"></script>
  </body>

</html>