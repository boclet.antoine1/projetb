#Galerie d'image d'un street artiste : Banksy

Réalisez un mur d'exposition des oeuvres de Banksy (une vingtaine) dans le style 'Pinterest' à l'aide du plugin Masonry.
En haut de page affichez un *Hero* faisant la promo du film : ["Faites le mur"](https://www.youtube.com/watch?v=kcRuHVbmcF0). Au click, vous afficherez la bande annonce dans une *modal*.

---

Vous devrez travailler en groupe à l'aide de `GIT` et `npm`. Pensez bien à l'organisation du projet et à communiquer.
Pour rappel, certains fichiers sont dispensables, il faut qu'ils soient ignorés (exemple : node_modules).

N'utilisez ni CDN, ni téléchargement manuel, à l'exception des images.

Chaque projet doit avoir un pied de page avec un zone d'inscription à une newsletter. Affichez un message de confirmation lors de l'inscription en utilisant [tingle.js](https://robinparisi.github.io/tingle/). 

Enfin, une série de plugins sont à intégrer **obligatoirement** pour ce projet : 

##Plugins
- [Masonry-layout](https://www.npmjs.com/package/masonry-layout)
- [LightGallery (version Jquery)](http://sachinchoolur.github.io/lightGallery/)
- [Modal-video](https://github.com/appleple/modal-video)
